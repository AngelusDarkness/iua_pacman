﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoGrid : MonoBehaviour {

	[SerializeField] private int _width;
	[SerializeField] private int _height;
	[SerializeField] private Transform _origin;
	[SerializeField] private Vector3 _cellSize;

	[SerializeField] private bool _show = true;
	void OnDrawGizmos() {
		if(_show) {
			Vector3 center = _origin.position;
			for(int i=0; i<_width; i++) {
				for(int j=0; j<_height; j++) {
					center.x = i * _cellSize.x;
					center.y = j * _cellSize.y;
					Gizmos.DrawWireCube(_origin.position + center,_cellSize);
				}
			}
		}
	}
}
