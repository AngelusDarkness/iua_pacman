﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PelletCreator : MonoBehaviour {
	//Editor Members
	[SerializeField] private GameObject _pelletPrefab;

	//Private Members
	private List<GameObject> _pelletsList = new List<GameObject>();

	public void AddPelletAt(Vector3 position) {
		GameObject pelletGO = PrefabUtility.InstantiatePrefab(_pelletPrefab) as GameObject;
		pelletGO.transform.position = position;
		pelletGO.transform.parent = transform;
		
		_pelletsList.Add(pelletGO);
	}

	public void Clear() {
		for(int i=0; i<_pelletsList.Count; i++) {
			DestroyImmediate(_pelletsList[i]);
		}
		_pelletsList.Clear();
	}
}
