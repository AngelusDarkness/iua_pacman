﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	//Private Members
	private int _currentScore;
	private static GameManager _instance;
	//Editor Members
	[SerializeField] private Text _scoreText;

	//Public Members
	public static GameManager Instance {
		set {
			_instance = value;
		}
		get {
			if(_instance == null) {
				_instance = GameObject.FindObjectOfType<GameManager>() as GameManager;
				if(_instance == null) {
					_instance = new GameObject().AddComponent<GameManager>();

				}
			}

			return _instance;
		}
	}
	

	// Use this for initialization
	void Start () {
		_currentScore = 0;
	}
	
	public void UpdateScore(int score) {
		_currentScore += score;
		_scoreText.text = string.Format("{0:00}",_currentScore);
	}
}
