﻿using UnityEngine;

public class Portal : MonoBehaviour {
	
	//Private Members
	private Collider _collider;	
	//Editor Members
	[SerializeField] Portal _destinationPortal;

	void Start() {
		_collider = GetComponent<Collider>();
	}
	void OnTriggerEnter(Collider col) {
		if(col.CompareTag("Player")) {			
			_destinationPortal.DeactivateCollider();
			col.GetComponent<Pacman>().PortalTo(_destinationPortal.transform.position);
			col.transform.position = _destinationPortal.transform.position;
			col.transform.rotation = _destinationPortal.transform.rotation;
		}
	}

	public void DeactivateCollider() {
		_collider.enabled = false;
		Invoke("ActivateCollider", 0.5f);
	}

	void ActivateCollider() {
		_collider.enabled = true;
	}
}
