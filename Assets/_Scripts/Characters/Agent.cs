﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour {


	protected enum Directions {
		kUp,
		kDown,
		kLeft,
		kRight,
		kNone
	};

	protected Directions _direction = Directions.kNone;		
	protected delegate void UpdateState();
	protected UpdateState _currentState;
	protected float _interval = 0;
	protected Vector2 _nextPosition;
	protected bool _canMove = true;
	protected bool _isMoving = false;	

	[SerializeField] protected float _moveStep = 1f;
	[SerializeField] protected float _moveSpeed = 1f;
	

	// Use this for initialization
	public virtual void Start () {		
		_nextPosition = transform.position;
		_canMove = true;	
	}
	
	// Update is called once per frame
	public virtual void Update () {
		if(_currentState != null)
			_currentState();
	}

}
