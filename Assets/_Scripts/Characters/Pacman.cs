﻿using UnityEngine;

public class Pacman : Agent { 

	[SerializeField] private float _health = 3;


	// Use this for initialization
	public override void Start () {
		base.Start();
			
		Move();
	}

	void Move() {
		_currentState = MoveState;
	}
	void MoveState() {		
		if(_canMove && Input.GetAxis("Vertical") > 0) {
			_direction = Directions.kUp;
			CalculateStep();	
		}
		
		if(_canMove &&Input.GetAxis("Vertical") < 0) {
			_direction = Directions.kDown;
			CalculateStep();			
		}

		if(_canMove && Input.GetAxis("Horizontal") < 0) {
			_direction = Directions.kLeft;
			CalculateStep();			
		}

		if(_canMove && Input.GetAxis("Horizontal") > 0) {
			_direction = Directions.kRight;
			CalculateStep();
		}

		if(_isMoving) {
			transform.position = Vector3.Lerp(transform.position, _nextPosition,_interval);

			if(_interval <= 1) {
				_canMove = false;
				_interval += _moveSpeed * Time.deltaTime;
			}
			else {
				_isMoving = false;
				_canMove = true;
				CalculateStep();				
			}
		}
	}

	void CalculateStep() {
		RaycastHit hit;
		Collider collider = null;
		string tag = string.Empty;	
		switch(_direction) {
			
			case Directions.kDown:				
				if (Physics.Raycast(transform.position, Vector3.down, out hit, _moveStep)) {
					collider = hit.collider;
					tag = hit.collider.tag;			
				}

				if(tag.CompareTo("Wall") != 0) {
					TriggerAction(collider);
					_nextPosition = new Vector2(transform.position.x,
												transform.position.y - _moveStep);
					transform.localEulerAngles = Vector3.back * 90;					
				}
			break;
			case Directions.kUp:
				if (Physics.Raycast(transform.position, Vector3.up, out hit, _moveStep)) {
					collider = hit.collider;
					tag = hit.collider.tag;						
				}

				if(tag.CompareTo("Wall") != 0) {
					TriggerAction(collider);
					_nextPosition = new Vector2(transform.position.x,
												transform.position.y + _moveStep);
					transform.localEulerAngles = Vector3.forward * 90;
				}
			break;			
			case Directions.kRight:
				if (Physics.Raycast(transform.position, Vector3.right, out hit, _moveStep)) {
					collider = hit.collider;
					tag = hit.collider.tag;				
				}

				if(tag.CompareTo("Wall") != 0) {
					TriggerAction(collider);
					_nextPosition = new Vector2(transform.position.x + _moveStep,
												transform.position.y);
					transform.localEulerAngles = Vector3.zero;
				}
			break;
			case Directions.kLeft:
				if (Physics.Raycast(transform.position, Vector3.left, out hit, _moveStep)) {
					collider = hit.collider;
					tag = hit.collider.tag;				
				}

				if(tag.CompareTo("Wall") != 0) {
					TriggerAction(collider);
					_nextPosition = new Vector2(transform.position.x - _moveStep,
												transform.position.y);
					transform.localEulerAngles = Vector3.down * 180;
				}
			break;
		}
		_interval = 0;
		_isMoving = true;
	}

	void TriggerAction(Collider collider) {
		if(collider != null) {
			switch(collider.tag) {
				case "Pellet":
					GameManager.Instance.UpdateScore(1);
					Destroy(collider.gameObject);
				break;
			}
		}
	}

	public void PortalTo(Vector3 position) {		
		_currentState = null;
		_nextPosition = position;
		Move();		
	}
}

