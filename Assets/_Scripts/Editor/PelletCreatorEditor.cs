﻿using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(PelletCreator))]
public class PelletCreatorEditor : Editor {

	void OnSceneGUI() {

		if(Event.current.button == 1 && Event.current.type == EventType.MouseUp) {
			Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			RaycastHit hit;
			Debug.Log("OnSceneGUI");
			if(Physics.Raycast(ray, out hit)){
				PelletCreator creator = target as PelletCreator;
				creator.AddPelletAt(hit.point);
			}
		}
	}

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		PelletCreator creator = target as PelletCreator;
		if(GUILayout.Button("Clear")) {
			creator.Clear();
		}
	}
}
